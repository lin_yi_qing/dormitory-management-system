# 宿舍管理系统

#### 介绍
项目: 宿舍管理系统<br> 
技术栈: Vue2 + JDK11 + SpringBoot2.6 + MP + Swagger3 + MySQL + Redis + MiniIO + Docker <br> 
目前已不免费提供，需要获取源码可以入群找群主私聊 <br> 
源码收费: **¥299**<br> 
如果需要提供线上环境，可以临时提供线上环境并且搭建上线收费(或帮忙搭建到云服务器): **¥399**<br> 
如果需要讲解按小时收费<br> 

## 想要定制化
如果需要定制化系统，可以加Q群私聊群主，或者写新系统<br>
对于系统完成的参与方案有两种：<br>
- 手把手教方式(这个方案定价会稍微多点)
- 直接编码方式<br>
对于具体的收费根据具体需求去收取，本人参与开发收取费用：**>=¥1800**<br>
具体价值根据需求定，其余勿扰，每次编码都是原创，并且风格不一样<br>
个人擅长的开发语言:<br>
    - **Go**
    - **Java**


## 如何通知
如果需要入群学习交流，随时欢迎，其余勿扰<br>
Q群: **995832569**<br>
B站号: **枫度柚子**<br>

## 线上地址
 - http://49.234.144.14:2000/dashboard

## 想要换一个系统参考
  - **仓库管理系统**: https://gitee.com/lin_yi_qing/inventory.git<br>

# 登录界面
![image.png](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/04e75f7143e64f5bada2cf204c3963c7~tplv-k3u1fbpfcp-watermark.image?)

# 管理员界面
## 首页
![image.png](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/4c50a5684dc14ed5a1f6c8e15852b0e5~tplv-k3u1fbpfcp-watermark.image?)

## 用户管理

![image.png](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/ecada021cb384875b34fdd5590d2ddaf~tplv-k3u1fbpfcp-watermark.image?)


![image.png](https://p9-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/9223483a9b2f400a9d8e006adfdc8b6c~tplv-k3u1fbpfcp-watermark.image?)

## 宿舍管理

![image.png](https://p9-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/c32cc1bb04234ba0b49ccab22480a359~tplv-k3u1fbpfcp-watermark.image?)

![image.png](https://p9-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/8d7c43f64dd34f6d8670b84d3613909f~tplv-k3u1fbpfcp-watermark.image?)

## 公告管理

![image.png](https://p6-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/a32d305baabe46aeb6ac4f766c93da46~tplv-k3u1fbpfcp-watermark.image?)

## 问题管理

![image.png](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/7ddeb1679b6a424d8ed9bf810e0f3a9c~tplv-k3u1fbpfcp-watermark.image?)

## 考勤管理

![image.png](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/91ea056511b748a9a25a0c8e2f49c8e9~tplv-k3u1fbpfcp-watermark.image?)


# 学生界面
## 首页
![image.png](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/30d7368f95b44ab4b1580bbd2b6520ab~tplv-k3u1fbpfcp-watermark.image?)

![image.png](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/9e8fff6fd619450fae4a3f5e0d4fcd0e~tplv-k3u1fbpfcp-watermark.image?)

## 我的宿舍

![image.png](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/30e8bec966e34d9d9adb4ef7e4a14a4c~tplv-k3u1fbpfcp-watermark.image?)

## 我的问题

![image.png](https://p9-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/d648b489c8f641298f6f8d1df3c45a0c~tplv-k3u1fbpfcp-watermark.image?)

## 问题管理

![image.png](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/2a5abb51b9924b87ac4504f9236b4c10~tplv-k3u1fbpfcp-watermark.image?)

## 个人中心

![image.png](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/13d0390ffd8c4097b0e8911a4cd53c93~tplv-k3u1fbpfcp-watermark.image?)


![image.png](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/f3cfd8262a004a81a9957fe959ab91b5~tplv-k3u1fbpfcp-watermark.image?)